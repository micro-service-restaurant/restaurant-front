import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {catchError, Observable, retry, tap, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PtRestHttpService {

  constructor(
    private httpClient: HttpClient
  ) { }

  get(host: string, url: string): Observable<Response>{
    return this.httpClient.get<Response>(host + url)
    .pipe(retry(1),
    catchError(this.handleError),
    tap((data: Response) => {
      console.log("GET ALL ERROR: " + data)
    }));
  }





  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    console.log(errorMessage);
    //return throwError(errorMessage);
    return throwError(() => errorMessage);
  }
}
