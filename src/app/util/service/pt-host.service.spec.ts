import { TestBed } from '@angular/core/testing';

import { PtHostService } from './pt-host.service';

describe('PtHostService', () => {
  let service: PtHostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PtHostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
