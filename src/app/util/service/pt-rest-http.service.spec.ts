import { TestBed } from '@angular/core/testing';

import { PtRestHttpService } from './pt-rest-http.service';

describe('PtRestHttpService', () => {
  let service: PtRestHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PtRestHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
