import { NavItem } from './../../util/menu.model';
import { Component, EventEmitter } from '@angular/core';

import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BreakpointObserver} from "@angular/cdk/layout";
import {MatSidenav} from "@angular/material/sidenav";
import {map, Observable, shareReplay} from "rxjs";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {
  propagar = new EventEmitter<string>();


  public loading!: boolean;
  public isAuthenticated!: boolean;
  public title!: string;

  public isBypass!: boolean;
  public mobile!: boolean;
  public isMenuInitOpen!: boolean;

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router,
              private _snackBar: MatSnackBar) {
  }


  private sidenav!: MatSidenav;

  public isMenuOpen = true;
  public contentMargin = 240;

  get isHandset(): boolean {
    //return this.breakpointObserver.isMatched('(max-width: 720px)');
    //return this.breakpointObserver.isMatched('(max-width: 960px)');
    return this.breakpointObserver.isMatched('(max-width: 1280px)');
  }

  //isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 720px)')
  //isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 960px)')
  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 1280px)')
    .pipe(
      map(result => result.matches),
      shareReplay(1),
    );

  // *********************************************************************************************
  // * LIFE CYCLE EVENT FUNCTIONS
  // *********************************************************************************************


  ngOnInit() {
    this.isMenuOpen = true;  // Open side menu by default
    this.title = 'Sistema ventas - ALVA FLORES ROOSBELTH EUFRAIN';
  }

  ngDoCheck() {
    if (this.isHandset) {
      this.isMenuOpen = false;
    } else {
      this.isMenuOpen = true;
    }
  }

  // *********************************************************************************************
  // * COMPONENT FUNCTIONS
  // *********************************************************************************************

  public openSnackBar(msg: string): void {
    this._snackBar.open(msg, 'X', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: 'notif-error'
    });
  }

  public onSelectOption(option: any): void {
    const msg = `Chose option ${option}`;
    this.openSnackBar(msg);

    /* To route to another page from here */
    // this.router.navigate(['/home']);
  }


  clicked: boolean = false;

  Clicked() {
    this.clicked = true;
  }

  procesaPropagar(mensaje:string) {
    console.log(mensaje);
  }

  menu1: NavItem [] = [
    {
      displayName: 'Escritorio',
      iconName: 'desktop_windows',
      route: '/olex',
    },
    {
      displayName: 'Administrador',
      /*iconName: 'description',*/
      iconName: 'pageview',
      children: [
        {
          displayName: 'Profesion',
          /*iconName: 'how_to_reg',*/
          iconName: 'school',
          route: '/control-pago/profesiones'
        },
        {
          displayName: 'Empleados',
          //iconName: 'find_in_page',
          iconName: 'nature_people',
          route: '/control-pago/empleados'
        },
        {
          displayName: 'Usuarios',
          /*iconName: 'how_to_reg',*/
          iconName: 'plagiarism',
          route: '/control-pago/usuarios'
        },
        {
          displayName: 'Roles',
          iconName: 'device_hub',
          route: '/control-pago/roles'
        }
      ]
    },
    {
      displayName: 'Registro',
      /*iconName: 'description',*/
      iconName: 'add_to_queue',
      children: [
        {
          displayName: 'Item3',
          iconName: 'post_add',
          route: '/olex/informes-opinion-legal-absolucion-consultas'
        },
        {
          displayName: 'Iten2',
          iconName: 'playlist_add',
          route: '/olex/registrar-dispositivos-normativos'
        }
      ]
    },
    {
      displayName: 'Sessión',
      //iconName: 'group',
      iconName: 'apps',
      children: [
        {
          displayName: 'Tipo Párametro',
          iconName: 'tune',
          route: '/olex/tipoParametro'
        },
        {
          displayName: 'Párametros Generales',
          iconName: 'business_center',
          route: '/olex/parametrosGeneral'
        },
        {
          displayName: 'Cerrar Sesión ',
          iconName: 'logout',
          route: '/'
        }
      ]
    }
  ];
}
